import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private navService: NavService) { }

  ngOnInit() {
    this.navService.setNavState(false);
  }

}
