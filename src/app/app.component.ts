import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {NavService} from './nav.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  navStateSub: Subscription;
  open = false;

  constructor(private navService: NavService, private router: Router) {
  }

  ngOnInit() {
    this.navStateSub = this.navService.getNavState().subscribe(state => this.open = state);
  }

  ngOnDestroy() {
    this.navStateSub.unsubscribe();
  }
}
