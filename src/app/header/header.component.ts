import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavService} from '../nav.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  openSub: Subscription;
  open = true;
  showGrade = false;

  constructor(private navService: NavService, private router: Router) {
  }

  ngOnInit() {
    this.openSub = this.navService.getNavState().subscribe(state => this.open = state);
    if (this.router.url === '/grades' || this.router.url === '/history') {
      this.showGrade = true;
    }
  }

  ngOnDestroy(): void {
    this.openSub.unsubscribe();
  }

}
