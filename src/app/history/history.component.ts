import {Component, OnInit} from '@angular/core';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  creditMode = true;
  mobile = false;

  constructor(private navService: NavService) { }

  ngOnInit() {
    if (window.screen.width < 678) {
      this.mobile = true;
    }
  }

  setCreditMode(mode: boolean) {
    this.creditMode = mode;
  }

}
