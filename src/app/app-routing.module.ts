import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {OverviewComponent} from './overview/overview.component';
import {GradesComponent} from './grades/grades.component';
import {DownloadsComponent} from './downloads/downloads.component';
import {HistoryComponent} from './history/history.component';
import {ImmersionComponent} from './immersion/immersion.component';


const routes: Routes = [
  {path: '', component: OverviewComponent},
  {path: 'login', component: LoginComponent},
  {path: 'grades', component: GradesComponent},
  {path: 'downloads', component: DownloadsComponent},
  {path: 'history', component: HistoryComponent},
  {path: 'immersion', component: ImmersionComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
