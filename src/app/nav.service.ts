import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  private navOpen = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  getNavState() {
    return this.navOpen.asObservable();
  }

  setNavState(state: boolean) {
    this.navOpen.next(state);
  }
}
