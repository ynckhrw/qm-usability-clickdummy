import { Component, OnInit } from '@angular/core';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-immersion',
  templateUrl: './immersion.component.html',
  styleUrls: ['./immersion.component.scss']
})
export class ImmersionComponent implements OnInit {

  constructor(private navService: NavService) { }

  ngOnInit() {
  }

}
